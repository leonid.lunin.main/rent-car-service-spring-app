package ua.ithillel.car.service.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ua.ithillel.car.service.entity.Car;

@Repository
public interface CarRepository extends CrudRepository<Car, Long> {
    Car findCarById(long id);
}
