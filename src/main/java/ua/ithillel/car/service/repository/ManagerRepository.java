package ua.ithillel.car.service.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ua.ithillel.car.service.entity.Manager;

import java.util.List;
import java.util.Optional;

@Repository
public interface ManagerRepository extends CrudRepository<Manager, Long> {
    Optional<Manager> findManagerByLoginAndPassword(String login, String password);

    List<Manager> findAll();

    Optional<Manager> findManagerById(long id);
}
