package ua.ithillel.car.service.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ua.ithillel.car.service.entity.Client;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {
    Optional<Client> findClientByLoginAndPassword(String login, String password);

    List<Client> findAll();

    Client getClientByLogin(String login);
}
