package ua.ithillel.car.service.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ua.ithillel.car.service.entity.Manager;
import ua.ithillel.car.service.repository.ManagerRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class ManagerService {
    private final ManagerRepository managerRepository;

    public List<Manager> getAllManagers() {
        List<Manager> managers = new ArrayList<>();
        managerRepository.findAll().forEach(managers::add);
        return managers;
    }

    public Optional<Manager> getManagerByLoginAndPassword(String login, String password) {
        return managerRepository.findManagerByLoginAndPassword(login, password);
    }

    public Optional<Manager> getManagerById(long id) {
        return managerRepository.findManagerById(id);
    }

    private long generateMaxId() {
        return getAllManagers().stream().map(Manager::getId).mapToInt(Long::intValue).max().orElseThrow();
    }

    public boolean verifyCredentials(String login, String password) {
        return getManagerByLoginAndPassword(login, password).isPresent();
    }

    public Manager getRandomManager() {
        Random rand = new Random();
        Long randomNum = rand.nextLong(generateMaxId());
        return getManagerById(randomNum).orElseGet(this::getRandomManager);
    }
}
