package ua.ithillel.car.service.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ithillel.car.service.entity.Car;
import ua.ithillel.car.service.entity.Order;
import ua.ithillel.car.service.repository.CarRepository;
import ua.ithillel.car.service.repository.OrderRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    private final CarRepository carRepository;

    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    @Transactional
    public void createOrder(Order order) {
        order.setId(generateMaxId() + 1);
        order.setDate(new java.util.Date());
        Car car = order.getCar();
        car.setAvailable(false);
        carRepository.save(car);
        orderRepository.save(order);
    }

    @Transactional
    public void deleteOrder(long id) {
        Car car = carRepository.findCarById(orderRepository.findOrderById(id).getCar().getId());
        car.setAvailable(true);
        carRepository.save(car);
        orderRepository.deleteById(id);
    }

    private long generateMaxId() {
        return getAllOrders().stream().map(Order::getId).mapToInt(Long::intValue).max().orElseThrow();
    }
}
