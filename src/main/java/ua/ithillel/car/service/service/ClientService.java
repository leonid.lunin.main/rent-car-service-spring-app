package ua.ithillel.car.service.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ua.ithillel.car.service.entity.Client;
import ua.ithillel.car.service.repository.ClientRepository;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ClientService {
    private final ClientRepository clientRepository;

    public Client getClientByLogin(String login) {
        return clientRepository.getClientByLogin(login);
    }

    public Optional<Client> getClientByLoginAndPassword(String login, String password) {
        return clientRepository.findClientByLoginAndPassword(login, password);
    }

    public boolean verifyCredentials(String login, String password) {
        return getClientByLoginAndPassword(login, password).isPresent();
    }
}
