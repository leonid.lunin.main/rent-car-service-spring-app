package ua.ithillel.car.service.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ua.ithillel.car.service.entity.Car;
import ua.ithillel.car.service.repository.CarRepository;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CarService {
    private final CarRepository carRepository;

    public List<Car> getAllCars() {
        List<Car> cars = new ArrayList<>();
        carRepository.findAll().forEach(cars::add);
        return cars;
    }

    public Car findCarById(long id) {
        return carRepository.findCarById(id);
    }

    public void createCar(Car car) {
        car.setId(generateMaxId() + 1);
        carRepository.save(car);
    }

    public void deleteCar(long id) {
        carRepository.deleteById(id);
    }

    private long generateMaxId() {
        return getAllCars().stream().map(Car::getId).mapToInt(Long::intValue).max().orElseThrow();
    }
}
