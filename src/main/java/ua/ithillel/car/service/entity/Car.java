package ua.ithillel.car.service.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@Entity
@ToString
@Table(name = "cars")
public class Car {
    @Id
    private long id;
    private String manufacturer;
    private String model;
    private int year;
    private int price;
    private boolean available;

    @OneToMany(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "car_id", updatable = false, insertable = false, nullable = false)
    private List<Order> orders;

}
