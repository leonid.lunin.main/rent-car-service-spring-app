package ua.ithillel.car.service.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import ua.ithillel.car.service.model.LoginForm;
import ua.ithillel.car.service.service.ClientService;
import ua.ithillel.car.service.service.ManagerService;

import javax.servlet.http.HttpSession;

@Controller
@RequiredArgsConstructor
@SessionAttributes("login")
public class LoginController {
    private final ClientService clientService;

    private final ManagerService managerService;

    @GetMapping(value = "/")
    public String index(Model model) {
        model.addAttribute("LoginForm", new LoginForm());
        model.addAttribute("status", false);
        return "index";
    }

    @PostMapping("/login")
    public String login(LoginForm loginForm, Model model, HttpSession session) {
        session.setAttribute("loginForm", loginForm);
        boolean authSucceed = false;
        model.addAttribute("isManager", loginForm.isManager());
        if (loginForm.isManager()) {
            authSucceed = managerService.verifyCredentials(loginForm.getLogin(), loginForm.getPassword());
            if (authSucceed) {
                return "redirect:/cars/";
            } else {
                model.addAttribute("LoginForm", loginForm);
                model.addAttribute("status", !authSucceed);
                return "index";
            }
        } else {
            authSucceed = clientService.verifyCredentials(loginForm.getLogin(), loginForm.getPassword());
            if (authSucceed) {
                return "redirect:/cars/";
            } else {
                model.addAttribute("LoginForm", loginForm);
                model.addAttribute("status", !authSucceed);
                return "index";
            }
        }
    }
}
