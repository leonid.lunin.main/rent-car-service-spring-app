package ua.ithillel.car.service.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ua.ithillel.car.service.entity.Order;
import ua.ithillel.car.service.model.LoginForm;
import ua.ithillel.car.service.service.CarService;
import ua.ithillel.car.service.service.ClientService;
import ua.ithillel.car.service.service.ManagerService;
import ua.ithillel.car.service.service.OrderService;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/orders")
@RequiredArgsConstructor
public class OrdersController {
    private final OrderService orderService;
    private final CarService carService;
    private final ClientService clientService;
    private final ManagerService managerService;

    @GetMapping("/")
    public String getOrders(Model model) {
        model.addAttribute("orders", orderService.getAllOrders());
        return "orders";
    }

    @PostMapping("/create")
    public String createOrders(@ModelAttribute("order") Order order, @RequestParam(name = "carId") Long carId, HttpSession session) {
        order.setCar(carService.findCarById(carId));
        order.setClient(clientService.getClientByLogin(((LoginForm) session.getAttribute("loginForm")).getLogin()));
        order.setManager(managerService.getRandomManager());
        orderService.createOrder(order);
        return "redirect:/cars/";
    }

    @PostMapping("delete/{id}")
    public String deleteOrder(@PathVariable("id") long id) {
        orderService.deleteOrder(id);
        return "redirect:/orders/";
    }

}
