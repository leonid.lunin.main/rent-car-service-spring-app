package ua.ithillel.car.service.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ua.ithillel.car.service.entity.Car;
import ua.ithillel.car.service.entity.Order;
import ua.ithillel.car.service.model.LoginForm;
import ua.ithillel.car.service.service.CarService;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/cars")
@RequiredArgsConstructor
public class CarsController {
    private final CarService carService;

    @GetMapping("/")
    public String getCars(Model model, HttpSession session) {
        model.addAttribute("cars", carService.getAllCars());
        model.addAttribute("order", new Order());
        model.addAttribute("isManager", ((LoginForm) session.getAttribute("loginForm")).isManager());
        return "carsList";
    }

    @RequestMapping("/add")
    public String addCar(Model model) {
        model.addAttribute("car", new Car());
        return "addCar";
    }

    @PostMapping("/create")
    public String createCar(@ModelAttribute("car") Car car) {
        carService.createCar(car);
        return "redirect:/cars/";
    }

    @PostMapping("delete/{id}")
    public String deleteCar(@PathVariable("id") long id) {
        carService.deleteCar(id);
        return "redirect:/cars/";
    }

}
